import Foundation

var input = ""
var statusCart: Bool = false
var arrayMenu: [Menu] = [Menu(menuID: "F01", menuName: "Nasi Padang"), Menu(menuID: "F02", menuName: "Gulai Kambing"), Menu(menuID: "F03", menuName: "Sapo Tahu"), Menu(menuID: "B01", menuName: "Air Mineral"), Menu(menuID: "B02", menuName: "Jus Stroberi")]
var arrayShoppingCart: [ShoppingCart] = []


func showMainMenu() {
    print("""

    =================================
       Academy's Cafe & Resto v2.0
    =================================

    Options:
    [1] Buy Food
    [2] Shopping Cart
    [x] Exit

    """)
    
    print("Your choice? ", terminator: "")
}

func showFoodMenu() {
    print("""
        
        Hi, we have 5 Food and Beverage options for you!
        ------------------------------------------------
        
        [\(arrayMenu[0].menuID)] \(arrayMenu[0].menuName)
        [\(arrayMenu[1].menuID)] \(arrayMenu[1].menuName)
        [\(arrayMenu[2].menuID)] \(arrayMenu[2].menuName)
        [\(arrayMenu[3].menuID)] \(arrayMenu[3].menuName)
        [\(arrayMenu[4].menuID)] \(arrayMenu[4].menuName)
        [Q] Back to Main Menu
        
        """, terminator: "")
    
    
}

func confirmOrder() {
    while input.lowercased() != "q"{
        print("\nYour F&B choice ? ", terminator: "")
        input = readLine() ?? ""
        var menuName: String = ""
        
        if input.lowercased() != "q"{
            
            for item in arrayMenu {
                if input.uppercased() == item.menuID {
                    menuName = item.menuName
                }
            }
            
            let shopItem = ShoppingCart(menuID: input.uppercased(), menuName: menuName)
            print("How many \(menuName) do you want to buy? ", terminator: "")
            let amount = readLine() ?? "1"
            shopItem.foodAmount = Int(amount) ?? 1
            
            //ngecek udah ada apa belom di cart, barang yang mau dibeli
            for items in arrayShoppingCart {
                if menuName == items.menuName{
                    statusCart = true
                    items.foodAmount = shopItem.foodAmount + items.foodAmount
                    break
                }else {
                    statusCart = false
                }
            }
            if statusCart == false {
                arrayShoppingCart.append(shopItem)
            }
            
        }
        shopCart()
    }
}


func shopCart() {
    if arrayShoppingCart.count == 0 {
        print("\nYour shopping cart is empty. Please buy something.")
    } else {
        print("\nShopping Cart (\(arrayShoppingCart.count) items): ")
        for item in arrayShoppingCart {
            print("\(item.foodAmount) \(item.menuName)")
        }
    }
}


while input.lowercased() != "x" {
    showMainMenu()
    input = readLine() ?? ""
    
    if input == "1" {
        showFoodMenu()
        confirmOrder()
        
        
    } else if input == "2" {
        shopCart()
        
    } else {
        print("Please input the correct choice option.")
    }
}

